## Non-Functional Requirements

Non-Functional requirements are an important part of any application, the list of Non-Functional Requirements are below.

1.	Performance

a.	Frame Rates: Kingdalore must run at a frame rate of at least 30 seconds, any lower may appear choppy to the user

b.	Loading Times: Kingdalore must have small loading times, the maximum loading time that will be allowed is about 1-2 minutes.

c.	Running: Kingdalore must be able to run for hours and hours and maintain stability, it shouldn�t degrade in quality the longer a user plays on the world.

d.	Response Time: Users interactions in the game should have a rapid response time to their actions, if they click their character response so be at minimum of .5 seconds to a maximum of 2 seconds.
2.	Usability

a.	Navigation: The amount of clicks a user uses to start and stop the game should never exceed 5. This will also include settings interactions.

b.	Controls: The Keyboard controls that the user uses must be completely natural feeling so that it is easy to use.
3.	Required Resources

a.	Folder Size: The initial install folder should try to refrain from being larger than 6 gigs in size.

b.	Ram Usage: The game shouldn�t never exceed the maximum of 2 gigs of ram and should be able to run on about 1 gig of ram.

4.	Platform

a.	Windows 10: Kingdalore will run the most efficiently on windows 10, Minimum Specs, (4 gigs Ram, and 4core Intel i5 CPU)

b.	Windows 7: Kingdalore will be able to run on windows 7 without much editing, Minimum Specs, (4 gigs Ram, and 4core Intel i5 CPU)

c.	Mac: Kingdalore will be able to run on the most current Mac OS (outside help may be required for this)

d.	Linux: Kingdalore will be able to run on some Linux systems, mainly Ubuntu
5.	Deployment

a.	Installation: Kingdalore must be easily installed with little user action required.

b.	Uninstalling: Kingdalore must be easily uninstalled in case user wants it to be.

6.	Maintainability

a.	Development Growth: Kingdalore must be able to be expanded upon later in development, so that more areas, characters, and items can be added easily.