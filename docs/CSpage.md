## Code Snippets

Below are code snippets from the various scripts that Kingdalore uses to manage all the objects and systems that it has.
The code snippets mainly focus on the player side.

#### Camera Follow Code Snippet
The Camera follow script is designed for the sole purpose of having the camera always keep the player central to its sight. This gives the player a good view of the player and the environment around them inside the demo.

```C#

    // Use this for initialization
    void Start()
    {
        //Move camera to players position
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10.0f);

        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - player.transform.position;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        transform.position = player.transform.position + offset;
    }
```

#### Player Health Manager
These methods from the player health manager, allow for other scripts to edit the players health, as well as when the player health turns to 0 the player is then deactivated till respawn. The get methods allow for other scripts to access the players health for later data.

```C#
	void Update () {
		if(playerCurrentHealth <= 0)
        {
            gameObject.SetActive(false);
        }
	}
    //public method for other scripts to activate
    public void HurtPlayer(int damageToGive)
    {
        playerCurrentHealth -= damageToGive;
    }

    public void SetMaxHealth()
    {
        playerCurrentHealth = playerMaxHealth;
    }
    //get method for other scripts to retreave
    public int getFullHealth()
    {
        return playerMaxHealth;
    }
    public int getCurrentHealth()
    {
        return playerCurrentHealth;
    }
```

#### Player Controller Script
The player controller script manages the movement of the player as well as very basic animation calls, the snippet below is a segment of the code that takes the keyboard input for the movement system.

```C#
      //check if player presses either WS or Up and Down arrows
            if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
            {
                //set players movement based on button that is pressed and have them move based on this new direction * movement speed * 2
                myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, Input.GetAxisRaw("Vertical") * moveSpeed * 2);
                //set playerMoveing true
                playerMoving = true;
                //set the last movement for animation
                lastMove = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, Input.GetAxisRaw("Vertical"));
            }
            //check if player presses either AD or Right and Left arrows
            if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
            {
                //set players movement based on button that is pressed and have them move based on this new direction * movement speed * 2
                myRigidbody.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed * 2 ,myRigidbody.velocity.y);
                //set playerMoveing true
                playerMoving = true;
                //set the last movement for animation
                lastMove = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, myRigidbody.velocity.y);
            }
```

            