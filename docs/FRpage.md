## Functional Requirements

Requirement IDs are formed based on the subtype of requirement. Example: Game Basics ID would be GB(instertNumbers).
Priority is split into three numbers 1= High Priority, 2= Medium Priority, 3= Future Priority

Definitions

Dynamic objects: Describes a moving object, such as NPC�s and Monsters
Static objects: Describes a static object, such as buildings and trees.
NPC: Non-Player Character, any character that is not directly controlled by the player.

### Game Basics
![Scheme](/img/Requirements1.png)
### Player Requirements
![Scheme](/img/Requirements2.png)
### Map Requirements
![Scheme](/img/Requirements3.png)
### Character Requirements
![Scheme](/img/Requirements4.png)
### Modes
![Scheme](/img/Requirements5.png)
### Collision Detection
![Scheme](/img/Requirements6.png)
### Story Requirements
![Scheme](/img/Requirements7.png)
### Quest Requirements
![Scheme](/img/Requirements8.png)
### UI Requirements
![Scheme](/img/Requirements9.png)
### Monster Requirements
![Scheme](/img/Requirements10.png)
### NPC (Non-Player Character) Requirements
![Scheme](/img/Requirements11.png)