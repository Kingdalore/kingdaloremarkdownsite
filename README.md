# KingdaloreMarkdownSite

## Abstract

The Kingdalore Demo is a two-dimensional role play game demo developed using pixel-based graphics, C# code and Unity. The demo will consist of the main graveyard starting area that the player can explore. This demo will include a partially animated player character that can move either up, down, right or left. This will be during inside the player C# movement script. This will also include the main areas the player can interact in, the Graveyard. The graveyard is where the player will get their first quests and the first taste of combat gameplay inside the demo. Here they will fight several basic monsters as well as explore. More technical parts of this demo will be the C# scripts that run everything from world generation, character movement, and inventory, monster AI, and sprites. 

## Video Overview
https://www.loom.com/share/8404643c386d499d89a4da15a156c78c

![Scheme](/img/loom.png)

## General Overview

Kingdalore was developed inside of Unity, with graphical elements made in Piskel and Gimp, and finally Code developed inside of Microsoft Visual Studio 2015.
The primary purpose of the project was to show to potential employers that even though I was taught Enterprise level development practices that I am capable of learning different frameworks, software IDEs and other systems.

## Functional Requirements
![Requirements] (https://www.preparationinfo.com/wp-content/uploads/2018/02/requirement-identification.png)

The page linked here
[Functional Requirements](/docs/FRpage.md)
contains the list of functional requirements that are defined for the development of the demo.

## Non-Functional Requirements
These are the non-functional requirements of Kingdalore.

![Scheme](/img/NonFunctionalRequirements.png)
## Technical Requirements
Below are the technical systems that are being used in this project:

1. Unity v2018.1.6f1 (64-bit) (Personal Version)
Use: Primary Game Development Platform

2. Visual Studio 2017
Use: Primary Coding Platform, Using C#

3. Gimp v2.10.6
Use: Primary Graphic Editor

4. Piskel v0.14.0
Use: Secondary Graphic Editor, Primary Sprite Maker

5. Sourcetree v2.4.7.0
Use: Program used to commit and push/pull to Bitbucket Repository

6. Bitbucket
Use: Repository for source files, graphics, documentation, etc.

7. C#
Use: Primary language being used for the player, map, tree, etc. scripts

## Code Snippets
[Code Snippets](/docs/CSpage.md)

## Logical System Design
![Scheme](/img/physicalArc.png)
![Scheme](/img/layerDesign.png)

## Physical System Design
![Scheme](/img/PhysicalSolution.png)

## Sitemap/UI Diagram
[Sitemap/UI](/docs/S-UIpage.md)

## Class Diagram
![Scheme](/img/ClassDiagram.png)
![Scheme](/img/ClassDiagrams.png)

## Future Ideas
There is lots of ideas for Kingdalore's future development. First being expnding the demo further to include two areas, more monsters, and a final boss. 
After the Demo is finished I adventually want to get a team together to continue development further into a full Role Play Game with a vast open game world to explore.
This Further expansion will include more enemies(Skeletons, Zombies, Spiders, etc.), more enviroments to explore(Empire,Swamps,Plains,ect.)and None Playable Characters to interact with.
All these ideas will take time, and I would need a team who can develop graphics, sound design and storyboarding.